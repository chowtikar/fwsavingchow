//
//  MenuListViewController.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/28/20.
//

import UIKit

class MenuListViewController: UIViewController {
    
    @IBOutlet weak var tableview: UITableView!
    
    var menuList: [(String, String)] = [("transaction", "Transaction"), ("bill", "Bill"), ("saving", "Saving"), ("report", "Report"), ("category", "Category"), ("backup", "Backup")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        tableview.delegate = self
        tableview.dataSource = self
        tableview.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MenuTableViewCell")
        tableview.separatorStyle = .singleLine
    }
}

extension MenuListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell else { fatalError() }
        let menu = menuList[indexPath.row] as (String, String)
        cell.menuImage.image = UIImage(named: menu.0)
        cell.titleLabel.text = menu.1
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

extension MenuListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

