//
//  MenuTableViewCell.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/24/20.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
    @IBOutlet var menuImage: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
    }
    
    private func setupCell() {
        titleLabel.textColor = .black
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
    }
    
    func setupData(image: String, title: String) {
       
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
