//
//  SplashscreenViewController.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/23/20.
//

import UIKit

class SplashscreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        routeToHome()
    }
    
    func routeToHome() {
        let home = TransactionHomeViewController()
        let nav = UINavigationController(rootViewController: home)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
}
