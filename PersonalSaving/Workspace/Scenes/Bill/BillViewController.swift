//
//  AddTransactionViewController.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/26/20.
//

import UIKit

class BillViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        title = "Bill"
        let menuButton = UIBarButtonItem(image: UIImage(named: "ic_menu"), style: .plain, target: self, action: #selector(menuTapped))
        let plusButton = UIBarButtonItem(image: UIImage(named: "ic_add"), style: .plain, target: self, action: #selector(plusTapped))
        self.navigationItem.leftBarButtonItem = menuButton
        self.navigationItem.rightBarButtonItem = plusButton
    }
    
    @objc func menuTapped() {
    }
    
    @objc func plusTapped() {
        let view = AddTransactionViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
}
