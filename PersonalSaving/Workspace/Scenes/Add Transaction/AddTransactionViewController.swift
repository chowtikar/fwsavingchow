//
//  AddTransactionViewController.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/26/20.
//

import UIKit

class AddTransactionViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        title = "Add Transaction"
        let backButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(backTapped))
        let addButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(addTapped))
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationItem.rightBarButtonItem = addButton
    }
    
    @objc func backTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addTapped() {
        
    }
}
