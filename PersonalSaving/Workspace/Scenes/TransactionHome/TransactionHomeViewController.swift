//
//  HomeViewController.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/24/20.
//

import UIKit

class TransactionHomeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    private func setupView() {
        title = "Transaction"
        let plusButton = UIBarButtonItem(image: UIImage(named: "ic_add"), style: .plain, target: self, action: #selector(plusTapped))
        self.navigationItem.rightBarButtonItem = plusButton
    }
    
    @objc func plusTapped() {
        let view = AddTransactionViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }

}
