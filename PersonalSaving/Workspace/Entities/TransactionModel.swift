//
//  TransactionModel.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/24/20.
//

import Foundation

class TransactionModel: Object {
    @objc dynamic var tranID = ""
    @objc dynamic var amount = 0.0
    @objc dynamic var note = ""
    @objc dynamic var timeStamp = NSDate()
    @objc dynamic var catID = ""
}
