//
//  SavingModel.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/24/20.
//

import Foundation

class SavingModel: Object {
    @objc dynamic var name = ""
    @objc dynamic var goal = 0.0
}
