//
//  CategoryModel.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/24/20.
//

import Foundation

class CategoryModel: Object {
    @objc dynamic var catID = ""
    @objc dynamic var type = ""
    @objc dynamic var name = ""
}
