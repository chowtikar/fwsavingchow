//
//  Utils.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/24/20.
//

import Foundation

struct Utils {
    private init() { }
    
    static func changeRootViewController(_ rootViewController: UIViewController, animated: Bool = true, from: UIViewController? = nil, completion: ((Bool) -> Void)? = nil) {
        let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) 
        if let window = window, animated {
            UIView.transition(with: window, duration: 0.175, options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                window.rootViewController = rootViewController
                window.makeKeyAndVisible()
                UIView.setAnimationsEnabled(oldState)
            }, completion: completion)
        } else {
            window?.rootViewController = rootViewController
        }
    }
    
}
