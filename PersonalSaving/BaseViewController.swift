//
//  AddTransactionViewController.swift
//  PersonalSaving
//
//  Created by chotikar p. on 11/28/20.
//

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMenu()
    }
    
    private func setupMenu() {
        let leftMenuView = SideMenuNavigationController(rootViewController: MenuListViewController())
        SideMenuManager.default.leftMenuNavigationController = leftMenuView
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width * 0.8
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAnimationFadeStrength = 0.5
        SideMenuManager.default.menuShadowOpacity = 0.8
        SideMenuManager.default.menuShadowRadius = 3.0
        addLeftButtonNavigation()
    }
    
    func addLeftButtonNavigation() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "ic_menu"), style: .plain, target: self, action: #selector(menuTapped))
        self.navigationItem.leftBarButtonItem = menuButton
    }
    
    @objc func menuTapped() {
        guard let leftMenuView = SideMenuManager.default.leftMenuNavigationController else { return }
        navigationController?.present(leftMenuView, animated: true, completion: nil)
    }

}
